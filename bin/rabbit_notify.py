#!/usr/bin/env python3


import pyinotify
from rabbitlistener.emitter import Emitter
import hashlib
import pathlib
from datetime import datetime
import yaml
import socket

script_dir = pathlib.Path(__file__).absolute().parent

class RabbitNotify:

    def __init__(self, uri, directory, topic='inc', hostname = socket.getfqdn() ):
        self.uri = uri
        self.directory = directory

        self.hostname = hostname
        self.topic = topic
        self.script_dir = pathlib.Path(__file__).absolute().parent
        self.log_dir = script_dir.parent / 'logs'

    def hash_file(filename):
        BUF_SIZE = 65536
        sha1 = hashlib.sha1()

        with open(filename, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                sha1.update(data)
        return sha1.hexdigest()


    def collect_info(self, filepath):
        d={}
        d['hostname'] = self.hostname
        d['directory'] = str(filepath.parent)
        d['file'] = filepath.name
        d['datetime'] = datetime.utcnow()
        d['path'] = str(filepath)
        d['size_bytes'] = filepath.stat().st_size
        #d['sha1sum'] = hash_file(filepath) 

        return d

    def emit_message(self, path):

        # Skip hidden files
        if path.name.startswith('.'):
            return
        dt = datetime.now()
        txt='[{datetime:%Y/%m/%dT%H:%M:%S}]: {path}'.format(datetime=dt, path=path)
        #print('{datetime:%Y/%m/%dT%H:%M:%S}: {path}'.format(datetime=datetime.now(), path=path))
        print(txt)
        
        try:
            logfile = self.log_dir / 'rabbit_notify-{datetime:%Y%m%d}.log'.format(datetime=dt)
            with logfile.open("a") as f:
                f.write(txt+'\n')
        except:
            print('Error writing log file.')

        
        msg = yaml.dump(self.collect_info(path), default_flow_style=True)
        try:
            self.emitter.send(self.topic, msg)
        except:
            self.emitter.connect_uri(self.uri)
            self.emitter.send(self.topic, msg)

    class EventHandler(pyinotify.ProcessEvent):
        
        def __init__(self, callback):
            self.callback = callback

        def process_IN_CLOSE_WRITE(self, event):
            self.callback(pathlib.Path(event.pathname))

        def process_IN_MOVED_TO(self, event):
            self.callback(pathlib.Path(event.pathname))

    def loop(self):
        self.emitter = Emitter()
        self.emitter.connect_uri(self.uri) 
        
        wm = pyinotify.WatchManager()
        mask = pyinotify.IN_MOVED_TO | pyinotify.IN_CLOSE_WRITE 

        handler = RabbitNotify.EventHandler(self.emit_message)
        notifier = pyinotify.Notifier(wm, handler)
        wdd = wm.add_watch(self.directory, mask, rec=True) 
        print('Watching directory: {directory}'.format(directory=self.directory))
        notifier.loop()


def parse_arguments():
    import argparse
    import os
    parser = argparse.ArgumentParser()
    env = {}
    
    parser.add_argument("--uri", default='', help="RabbitMQ server uri (amqp://<user>:<passw>@<hostname>:5672/sat-stream.")
    parser.add_argument("--topic", default='inc', help="Topic to publish under.")
    parser.add_argument("--hostname", default='', help="Hostname to publish in message.")
    parser.add_argument("--directory", default='', required=True, help="Directory to watch.")

    opts = parser.parse_args()
    params = vars(opts)
    if not params['uri']:
        params['uri'] = 'amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@{RABBITMQ_HOST}:5672/sat-stream'.format(**os.environ)
    if not params['hostname']:
        params['hostname'] = os.environ.get('HOSTNAME') if 'HOSTNAME' in os.environ else socket.getfqdn()
    return params


def main():
    params=parse_arguments()
    print('using uri: {uri}'.format(uri=params['uri']))
    rabbitnotify = RabbitNotify(uri=params['uri'], directory=params['directory'], topic=params['topic'], hostname=params['hostname'])
    rabbitnotify.loop()


if __name__ == '__main__':
    main()

