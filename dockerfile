# Barebones Apache installation on Ubuntu

FROM ubuntu:18.04

MAINTAINER Veðurstofan / Icelandic Meteorological Office


RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    git \
    inotify-tools 

RUN pip3 install --no-cache-dir git+https://git.vedur.is/eysteinn/rabbitlistener.git
RUN apt install -y vsftpd

RUN sed -i 's/anonymous_enable=NO/anonymous_enable=YES/g' /etc/vsftpd.conf


EXPOSE 20 21 21000-21010

RUN mkdir -p /var/run/vsftpd/empty

RUN pip3 install pyinotify==0.9.6

CMD ["/bin/bash"]
