# sat-notify

This docker image watches the ./inc directory and subdirectories (this can be replaced witha a softlink with the same name points to another directory or environmental variable, see below) and sends a message to a RabbitMQ message broker when a new file is created. In addition a ftp server is started that grants anonymous ftp access to the files under ./inc directory. This allowes for those who monitor the messages to download the files in a simple manner using the filepath given in the RabbitMQ message (see Rabbitlistener project).


Provide RabbitMQ hostname, username and password in an environemnt file named config.env in root directory next to docker-compose.yml.

Its recommended to provide hostname in the config file as well.

Example config.env:

```
RABBITMQ_USER=<username>
RABBITMQ_PASS=<password>
RABBITMQ_HOST=<rabbitmq hostname>
RABBITMQ_TOPIC=<topic to publish to>
HOSTNAME=<local hostname>
```

There are three ways of mounting the right incoming data volume:
 
 - Simplest is to export variable 'INC_DIR' to point to the right data dircetory:  `export INC_DIR=<incdir>; docker-compose up -d`
 - Change inc mount in the docker-compose.yml
 - Make a softlink `ln -s <incdir> ./inc`, replace <incdir> with the actual incoming data directory



To build:
`docker-compose build sat-notify`


To start the docker after changing the ./inc link:
`docker-compose up -d`


